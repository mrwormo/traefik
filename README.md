1. Installer htpasswd pour générer des mots de passe :

`sudo apt-get update && sudo apt-get install apache2-utils`

2. Créer l'arborescence : 

`mkdir -p /app/traefik/{ssl,conf.d/middlewares,logs} /app/collabora `

3. Créer les volumes Docker :

`docker volume create redis && docker volume create nextcloud && docker volume create mariadb && docker volume create redis`

4. Créer le network docker :

`docker network create traefik`

5. Compléter les fichier `.env`, `auth.yml` et `docker-compose.yml`


